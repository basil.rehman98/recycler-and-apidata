package com.example.retrofit_recycle

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofit_recycle.Model.Albums
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .build()
        val Holderapi = retrofit.create(holderapi ::class.java )
        val mycall : Call<List<Albums>> = Holderapi.getalbums()
        val recycle = findViewById<View>(R.id.recycle_view) as RecyclerView
        recycle.layoutManager = LinearLayoutManager(this)


       // val Holderapi = holderapi.create().getalbums()
        mycall.enqueue(object : Callback<List<Albums>>{
            override fun onResponse(call: Call<List<Albums>>, response: Response<List<Albums>>) {

                if (response?.body()!= null){


                    val Albumss : List<Albums> = response.body()!!
                    var myList = Albumss.toCollection(ArrayList())

                    var stringbuilder = StringBuilder()
                    Albumss.forEach { albums ->
                        stringbuilder.append(albums.userId)
                        stringbuilder.append(albums.id)
                        stringbuilder.append(albums.title)
                    }
                        val recycleradapter = CustomAdapter(myList)
                    recycle.adapter = recycleradapter
                }
            }

            override fun onFailure(call: Call<List<Albums>>, t: Throwable) {
                Log.e("failure","try again")
            }

        })
    }
}